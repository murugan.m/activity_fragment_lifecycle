package com.example.fisrtsampleapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import com.example.fisrtsampleapp.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
//        setContentView(R.layout.activity_main)
        setContentView(binding.root)

        val button = binding.myButton
//        val button = findViewById<Button>(R.id.myButton)
        println("Printed my First log")
        button.setOnClickListener {
            println("Button Clicked")
            Toast.makeText(this@MainActivity, "You clicked me.", Toast.LENGTH_SHORT).show()
            if (button.text == "You clicked me.") {
                button.text = "Button"
            } else {
                button.text = "You clicked me."
            }
//            val fragment = FirstFragment()
//            supportFragmentManager.beginTransaction().replace(R.id.framLayoutId,fragment).commit()

            val intent = Intent(this,SecondActivity::class.java)
            intent.putExtra("id_value", 100)
intent.putExtra("id_name", "chennai guy")
            startActivity(intent)

        }
    }

    override fun onStart() {
        super.onStart()
        println("MainActivity Inside ${object{}.javaClass.enclosingMethod.name}")
    }

    override fun onContentChanged() {
        super.onContentChanged()
        println("MainActivity Inside ${object{}.javaClass.enclosingMethod.name}")
    }

    override fun onStop() {
        super.onStop()
        println("MainActivity Inside ${object{}.javaClass.enclosingMethod.name}")
    }

    override fun onPause() {
        super.onPause()
        println("MainActivity Inside ${object{}.javaClass.enclosingMethod.name}")
    }

    override fun onResume() {
        super.onResume()
        println("MainActivity Inside ${object{}.javaClass.enclosingMethod.name}")
    }

    override fun onRestart() {
        super.onRestart()
        println("MainActivity Inside ${object{}.javaClass.enclosingMethod.name}")
    }

    override fun onPostResume() {
        super.onPostResume()
        println("MainActivity Inside ${object{}.javaClass.enclosingMethod.name}")
    }

    override fun onResumeFragments() {
        super.onResumeFragments()
        println("MainActivity Inside ${object{}.javaClass.enclosingMethod.name}")
    }

    override fun onDestroy() {
        super.onDestroy()
        println("MainActivity Inside ${object{}.javaClass.enclosingMethod.name}")
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        println("MainActivity Inside ${object{}.javaClass.enclosingMethod.name}")
    }
}